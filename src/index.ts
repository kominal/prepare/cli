import { error, info, warn } from '@kominal/lib-node-logging';
import { exec } from 'child_process';
import { copyFileSync, existsSync, mkdirSync, readdirSync, readFileSync, renameSync, statSync, writeFileSync } from 'fs';
import { exit } from 'process';
import simpleGit from 'simple-git';
import { dirSync } from 'tmp';

const templateFolder = dirSync().name;
const variables = new Map<string, string>();

export async function start() {
	const templatePath = process.argv[2];
	info(`Running Kominal Prepare for '${templatePath}'...`);

	parseCommandLineArguments();

	info(`Downloading template...`);
	await simpleGit().clone(`https://gitlab.com/kominal/prepare/templates/${templatePath}.git`, `${templateFolder}`);

	const statements = readFileSync(`${templateFolder}/Preparefile`).toString('utf-8').split(/\r?\n/);

	for (const statement of statements) {
		await executeStatement(statement);
	}
}

function parseCommandLineArguments() {
	const args = process.argv.slice(2);

	let variableName = undefined;
	for (const arg of args) {
		if (arg.startsWith('--')) {
			variableName = arg.slice(2);
			continue;
		}

		if (variableName) {
			variables.set(variableName, arg);
			variableName = undefined;
		}
	}
}

async function executeStatement(statement: string) {
	if (statement.length === 0) {
		return;
	}

	info(`Executing: ${statement}`);

	const [command, ...args] = parseStatementParts(statement);

	if (command === 'RUN') {
		await executeStatementRun(args);
	} else if (command === 'DEFAULT') {
		await executeStatementDefault(args);
	} else if (command === 'WORKDIR') {
		await executeStatementWorkdir(args);
	} else if (command === 'EXTRACT') {
		await executeStatementExtract(args);
	} else if (command === 'MOVE') {
		await executeStatementMove(args);
	} else if (command === 'REPLACE') {
		await executeStatementReplace(args);
	} else if (command === 'REQUIRE') {
		await executeStatementRequire(args);
	} else {
		warn(`Unknown command: ${command}`);
	}
}

function parseStatementParts(statement: string) {
	const args: string[] = [];

	let arg = 0;
	let quoted = false;
	let escaped = false;
	for (const c of statement) {
		if (c === '"') {
			if (escaped) {
				args[arg] += c;
				escaped = false;
			} else {
				if (quoted) {
					if (args[arg] && args[arg].length > 0) {
						arg++;
					}
				}
				quoted = !quoted;
			}
		} else if (c === '\\') {
			if (!escaped) {
				escaped = true;
				continue;
			} else {
				if (!args[arg]) {
					args[arg] = '';
				}
				args[arg] += c;
			}
		} else if (c === ' ') {
			if (quoted) {
				if (!args[arg]) {
					args[arg] = '';
				}
				args[arg] += c;
			} else {
				if (args[arg] && args[arg].length > 0) {
					arg++;
				}
			}
		} else {
			if (!args[arg]) {
				args[arg] = '';
			}
			args[arg] += c;
		}
		escaped = false;
	}

	return args;
}

function hydrate(statement: string): string {
	for (const [key, value] of variables) {
		statement = statement.replace(new RegExp(`{${key}}`, 'g'), value);
	}
	return statement;
}

async function executeStatementRun(args: string[]) {
	const childProcess = exec(hydrate(args.join(' ')));
	await new Promise((resolve) => childProcess.on('exit', resolve));
}

async function executeStatementDefault(args: string[]) {
	const [key, value] = args;
	if (!variables.has(key)) {
		variables.set(key, value);
	}
}

async function executeStatementWorkdir(args: string[]) {
	if (args.length === 0) {
		return;
	}
	const workDir = `${process.cwd()}/${hydrate(args[0])}`;

	if (!existsSync(workDir)) {
		mkdirSync(workDir, { recursive: true });
	}

	process.chdir(workDir);
}

async function executeStatementExtract(args: string[]) {
	const sourcePath = `${templateFolder}/${hydrate(args[0])}`;
	const targetPath = args.length === 2 ? `${process.cwd()}/${hydrate(args[1])}` : process.cwd();

	if (!existsSync(sourcePath)) {
		warn(
			`Could not extract content of folder '${hydrate(args[0])}' to '${
				args.length === 2 ? hydrate(args[1]) : '.'
			}': Source folder does not exists`
		);
		return;
	}

	extract(sourcePath, targetPath);
}

function extract(sourcePath: string, targetPath: string) {
	const stats = statSync(sourcePath);

	if (stats.isDirectory()) {
		mkdirSync(targetPath, { recursive: true });
		for (const path of readdirSync(sourcePath)) {
			extract(`${sourcePath}/${path}`, `${targetPath}/${path}`);
		}
	} else {
		if (existsSync(targetPath)) {
			warn(`Could not extract file '${sourcePath}' to '${targetPath}': File already exists`);
			return;
		}

		copyFileSync(sourcePath, targetPath);
	}
}

async function executeStatementMove(args: string[]) {
	const sourcePath = `${process.cwd()}/${hydrate(args[0])}`;
	const targetPath = `${process.cwd()}/${hydrate(args[1])}`;

	if (existsSync(targetPath)) {
		warn(`Could not move file '${hydrate(args[0])}' to '${hydrate(args[1])}': File already exists`);
		return;
	}

	renameSync(sourcePath, targetPath);
}

async function executeStatementReplace(args: string[]) {
	const path = `${process.cwd()}/${hydrate(args[0])}`;

	if (!existsSync(path)) {
		warn(`Could not modify content of file '${hydrate(args[0])}': File does not exists`);
		return;
	}

	const source = hydrate(args[1]);
	const target = hydrate(args[2]);

	let content = readFileSync(path).toString('utf-8');

	content = content.replace(new RegExp(source, 'g'), target);

	writeFileSync(path, content, { encoding: 'utf-8' });
}

async function executeStatementRequire(args: string[]) {
	let failed = false;

	for (const arg of args) {
		if (!variables.has(arg)) {
			failed = true;
			error(`Missing required parameter: ${arg}`);
		}
	}

	if (failed) {
		exit(1);
	}
}

start();
